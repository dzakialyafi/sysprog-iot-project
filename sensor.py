import pigpio
import DHT22
from time import sleep
import Rpi.GPIO as GPIO
import request
import sys
import os
import re
from email.mime.text import MIMEText

GPIO.setwarning(False)
GPIO.setmode(GPIO.BOARD)
GPIO.setup(11. GPIO.OUT)
GPIO.setup(13, GPIO.IN)
GPIO.setup(15, GPIO.OUT)
pi = pigpio.pi()
dht22 = DHT22.sensor(pi, 27)
dht22.trigger()

on_status = 0
off_status = 0

def kirimemail(temp):
	SMTPserver = 'smtp.gmail.com'
	sender =     'mmmburgerr@gmail.com'
	destination = [mail]

	USERNAME = "mmmburgerr@gmail.com"
	PASSWORD = "burgermmm"

	# typical values for text_subtype are plain, html, xml
	text_subtype = 'plain'


	content="Your temperature is " + temp

	subject="Sent from Python"

	try:
	    msg = MIMEText(content, text_subtype)
	    msg['Subject']=       subject
	    msg['From']   = sender # some SMTP servers will do this automatically, not all

	    conn = SMTP(SMTPserver)
	    conn.set_debuglevel(False)
	    conn.login(USERNAME, PASSWORD)
	    try:
	        conn.sendmail(sender, destination, msg.as_string())
	    finally:
	        conn.quit()

	except Exception, exc:
	    sys.exit( "mail failed; %s" % str(exc) ) # give a error message

def read_():
	dht22.trigger()
    t = "%.2f" % (dht22.temperature())
    return(t)

while True:
	print("Waiting for input")
	temp = read_()
	try:
		response = requests.urlopen('https://vinidictas.00webhostapp.com/buttonStatus.txt')
		status = response.read()
		stat = status.split(" ")
		mail = stat[2]

	if stat[1] == 'AUTO':
		if "25.00" <= temp < "30.00" or temp > "30.00":
			print('Device ON')
		    GPIO.output(11, False)
		    GPIO.output(15, HIGH)

			if on_status == 0:
				kirimemail(temp)
		    	on_status += 1
		    	if off_status != 0:
		    		off_status -= 1

		elif temp < "25.00":
			print('Device OFF')
			GPIO.output(11, True)
			GPIO.output(15, LOW)

			if off_status == 0:
				kirimemail(temp)
		    	off_status += 1
		    	if on_status != 0:
		    		on_status -= 1


print(temp)
sleep(3)