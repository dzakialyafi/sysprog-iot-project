import Rpi.GPIO as GPIO
import requests


GPIO.setwarning(False)
GPIO.setmode(GPIO.BOARD)
GPIO.setup(11, GPIO.OUT)
GPIO.setup(15, GPIO.OUT)

while True:
	print("Waiting for input")
	try:
		response = requests.urlopen('https://vinidictas.00webhostapp.com/buttonStatus.txt')
		status = response.read()
		stat = status.split(" ")

	if stat[1] == 'MANUAL':
		if stat[0] == "ON":
			print('Device ON')
		    GPIO.output(11, False)
		    GPIO.output(15, HIGH)
		elif stat[0] == "OFF":
			print('Device OFF')
			GPIO.output(11, True)
			GPIO.output(15, LOW)
sleep(3)