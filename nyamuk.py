import Rpi.GPIO as GPIO
import requests


GPIO.setwarning(False)
GPIO.setmode(GPIO.BOARD)
GPIO.setup(18, GPIO.OUT)

while True:
	print("Waiting for input")
	try:
		response = requests.urlopen('https://vinidictas.00webhostapp.com/buttonNyamuk.txt')
		status = response.read()
		stat = status.split(" ")

		if stat[0] == "ON":
			print('Device ON')
		    GPIO.output(18, False)
		elif stat[0] == "OFF":
			print('Device OFF')
			GPIO.output(18, True)
sleep(3)